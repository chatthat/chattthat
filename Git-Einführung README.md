1. Git installieren:
    Windows: Download unter [https://gitforwindows.org/](https://gitforwindows.org/)
    Linux/Ubuntu: Konsole öffnen, dann Befehl `sudo apt-get install git` eingeben und nach Passworteingabe mit "y" bestätigen
2. Dem GitLab-Account einen SSH-Key hinzufügen:
    -> Befehl `ssh-keygen -t ed25519 -C "eure@emailadresse.de"` eingeben
    -> Enter drücken, bis die Kommandzeile wieder verwendbar ist
    -> Befehl `cat /home/NUTZERNAME/.ssh/id_ed25519.pub` mit eurem Nutzernamen eingeben
    -> den Text, der ausgegeben wird, auswählen (sowas wie `ssh-ed25519 AAsaihggwon2q243ht38hgwvw2438z34z98634ihg2g1k eure@emailadresse.de`), dann rechtsklick, dann "copy"
    -> Auf [https://gitlab.com/profile/keys](https://gitlab.com/profile/keys) den Text von Oben ins große Feld eingeben, dann unten auf "Add Key" drücken
3. Sich bei Git "anmelden"
    -> `git config --global user.email EMAIL` mit eurer richtigen emailadresse statt EMAIL eingeben
    -> `git config --global user.name NUTZERNAMEN` mit eurem richtigen Gitlab-Nutzernamen statt NUTZERNAMEN eingeben
4. unser Projekt herunterladen ("clonen")
    -> `cd /home/NUTZERNAME/Documents/` mit eurem Nutzernamen eingeben
    -> `git init` eingeben, dann `git clone git@gitlab.com:chatthat/chattthat.git` eingeben.
5. jetzt könnt ihr den flexBB-Ordner in /opt/lampp kopieren, siehe README.md