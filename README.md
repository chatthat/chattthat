* [ ]  # chattthat

online-kummerkasten / austausch-forum

Den Server aufsetzen:
1. XAMPP herunterladen (Die Server-Software) - [https://sourceforge.net/projects/xampp/](https://sourceforge.net/projects/xampp/)
2. Installation:
WINDOWS-Installation:
    1.Den XAMPP-Installer ausführen
     -> Core-Dateien und Developer Files ankreuzen
     -> Bitnami Modules werden nicht verwendet, muss man sich nicht durchlesen.
LINUX-Installation:
    1. ein terminal öffnen (in den programmen nach "terminal" oder "tilix" suchen)
    2. "in den Downloads-Ordner gehen (Befehl: `cd ~/home/NUTZERNAME/Downloads/`") (NUTZERNAME ist der Name vor dem @-Zeichen am anfang der Konsole)(das "~"-Zeichen bekommt man durch AltGr und + Taste drücken gleichzetig))
    3. die Rechte der Installer-Datei modifizieren (Befehl: `sudo chmod +x xampp-linux-UNDSOWEITER`), ihr könnt einfach xampp eintippen und dann Tab drücken, dann Enter
    4. die Installer-Datei ausführen (Befehl: `sudo ./xampp-UNDOSWEITER`) (ihr könnt wieder Tab zum Vervollständigen drücken
     -> Core-Dateien und Developer Files ankreuzen
     -> Bitnami Modules werden nicht verwendet, muss man sich nicht durchlesen.
    5. Wenn die Installation fertig ist, in den Installationsordner wechseln (Befehl: `cd /opt/lampp`)
    6. net-tools installieren, damit der Server funktioniert (Befehl: `sudo apt-get install net-tools`) 
    7. Dem normalen Nutzer Zugriff auf die Serverordner geben, d.h.
     -> `sudo groupadd xamppusers`
     -> `sudo usermod -a -G NUTZERNAMEN xamppusers`
     -> `sudo chown root.xamppusers htdocs`
     -> `sudo chmod 775 htdocs`
     -> `sudo chmod +0777 /opt/lampp/htdocs/fluxbb-1.5.11/cache/`
     -> `sudo chmod +0777 /opt/lampp/htdocs/fluxbb-1.5.11/img/avatars/`
     -> Jetzt den Computer neu starten
    8. Den Server starten (Befehl: `sudo ./lampp start
    9. Den FluxBB-Ordner von gitlab herunterladen (siehe Git-Einführung README.md!)
    10. Den FluxBB-Ordner nach /opt/lampp/htdocs/ kopieren (am besten 2 Explorerfenster nebeneinander, um zu /opt/lampp/htdocs zu kommen, müsst ihr auf "Other Locations" und dann "Computer" gehen, da seht ihr dann den opt-Ordner)
3. Datenbank aufsetzen
    1. Im Browser in der Addresszeile `localhost/phpmyadmin` eingeben
    2. Oben auf "User Accounts" klicken
    3. Bei username: "root" und host name: "127.0.0.1" auf "edit privileges" klicken
    4. Oben auf "Change Password" klicken
    5. Als neues Passwort z.B. "password" eingeben, Nicht "Generate Password" klicken.
    6. rechts auf "Go" klicken
    7. Wieder oben auf "User Accounts" klicken, dann die roten "Any"-Nutzer bei den Kästchen nebendran anklicken, dann unten unter "Remove selected user accounts" auf der rechten Seite auf "Go" drücken um die Nutzer zu löschen. Die Warnungen die daraufhin kommen bestätigen
    8. In der Seitenleiste links oben auf "New" klicken, dann bei "Database Name" "chatthat_db" eingeben (ohne "") und auf "create" klicken
4. FluxBB aufrufen unter `localhost/fluxbb-1.15.11/index.php`, dann sollte hoffentlich alles gehen.
    -> Ihr könnt euch jetzt mit dem Nutzernamen "admin" und dem Passwort "this15saf3" anmelden und unter "Administration" alles mögliche am Server ändern.